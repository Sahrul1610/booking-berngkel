package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	private static List<BookingOrder> bookingOrderList = new ArrayList<>();
	private static Scanner scanner = new Scanner(System.in);


	public static void run() {
        boolean isLooping = true;
        do {
            displayStartMenu();
            int menuChoice = BengkelService.getStartMenuChoice();
            switch (menuChoice) {
                case 1:
                    BengkelService.login(listAllCustomers, scanner);
                    break;
                case 0:
                    System.out.println("Terima kasih telah menggunakan aplikasi Booking Bengkel.");
                    isLooping = false;
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan masukkan pilihan yang sesuai.");
            }
        } while (isLooping);
    }

	private static void displayStartMenu() {
        System.out.println("Aplikasi Booking Bengkel\n");
        System.out.println("1. Login");
        System.out.println("0. Exit");
        System.out.println();
    }

	
	public static void mainMenu(Customer loggedInCustomer) {
		String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
		int menuChoice = 0;
		boolean isLooping = true;
		
		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			System.out.println(menuChoice);
			
			switch (menuChoice) {
			case 1:
				//panggil fitur Informasi Customer
				PrintService.displayCustomerInfo(loggedInCustomer);
				break;
			case 2:
				//panggil fitur Booking Bengkel
				BengkelService.displayBookingMenu(listAllItemService,loggedInCustomer,scanner, bookingOrderList);
				break;
			case 3:
				//panggil fitur Top Up Saldo Coin
				BengkelService.displayTopUpMenu(loggedInCustomer, scanner);
				break;
			case 4:
				//panggil fitur Informasi Booking Order
				PrintService.displayBookingList(bookingOrderList, loggedInCustomer);
				break;
			default:
				System.out.println("Logout");
				isLooping = false;
				break;
			}
		} while (isLooping);
		
		
	}
	
}
