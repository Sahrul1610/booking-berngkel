package com.bengkel.booking.services;

import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.Customer;

public class Validation {

	public static String validasiInput(String question, String errorMessage, String regex) {
		Scanner input = new Scanner(System.in);
		String result;
		boolean isLooping = true;
		do {
			System.out.print(question);
			result = input.nextLine();

			if (result.matches(regex)) {
				isLooping = false;
			} else {
				System.out.println(errorMessage);
			}

		} while (isLooping);

		return result;
	}

	public static int validasiNumberWithRange(String question, String errorMessage, String regex, int max, int min) {
		int result;
		boolean isLooping = true;
		do {
			result = Integer.valueOf(validasiInput(question, errorMessage, regex));
			if (result >= min && result <= max) {
				isLooping = false;
			} else {
				System.out.println("Pilihan angka " + min + " s.d " + max);
			}
		} while (isLooping);

		return result;
	}

	public static double validasiNumber(String question, String errorMessage, String regex) {
		Scanner input = new Scanner(System.in);
		double result = -1;
		boolean isLooping = true;
		do {
			System.out.print(question);
			String userInput = input.nextLine();
			if (userInput.matches(regex)) {
				try {
					result = Double.parseDouble(userInput);
					isLooping = false;
				} catch (NumberFormatException e) {
					System.out.println(errorMessage);
				}
			} else {
				System.out.println(errorMessage);
			}
		} while (isLooping);
	
		return result;
	}
	

	public static Customer validateLogin(String customerId, String password, List<Customer> listAllCustomers) {
		for (Customer customer : listAllCustomers) {
			if (customer.getCustomerId().equals(customerId) && customer.getPassword().equals(password)) {
				return customer;
			}
		}
		return null; 
	}

	public static String validatePaymentMethod(Scanner scanner) {
		String paymentMethod;
		do {
			System.out.println("\nSilahkan Pilih Metode Pembayaran (Saldo Coin atau Cash)");
			paymentMethod = scanner.nextLine();
			if (!paymentMethod.equalsIgnoreCase("Saldo Coin") && !paymentMethod.equalsIgnoreCase("Cash")) {
				System.out.println("Input tidak valid. Harap masukkan 'Saldo Coin' atau 'Cash'.");
			}
		} while (!paymentMethod.equalsIgnoreCase("Saldo Coin") && !paymentMethod.equalsIgnoreCase("Cash"));
		return paymentMethod;
	}
}
